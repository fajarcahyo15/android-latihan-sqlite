package io.github.fajarcahyo15.crud_sqlite.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.github.fajarcahyo15.crud_sqlite.R;
import io.github.fajarcahyo15.crud_sqlite.database.PenggunaTableHandler;
import io.github.fajarcahyo15.crud_sqlite.model.Pengguna;
import io.github.fajarcahyo15.crud_sqlite.presenter.MainActivityPresenter;

/**
 * Created by root on 4/25/18.
 */

public class PenggunaAdapter extends RecyclerView.Adapter<PenggunaAdapter.MyViewHolder> {

    private Context mContext;
    private List<Pengguna> penggunas;
    private MainActivityPresenter mPresenter;

    public PenggunaAdapter(Context context, List<Pengguna> pengguna, MainActivityPresenter mainActivityPresenter) {
        mContext = context;
        penggunas = pengguna;
        mPresenter = mainActivityPresenter;
    }

    @Override
    public PenggunaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rl_pengguna,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PenggunaAdapter.MyViewHolder holder, final int position) {
        final Pengguna pengguna = penggunas.get(position);

        holder.tv_nama.setText(pengguna.getNama());
        holder.tv_level.setText(pengguna.getLevel());
        holder.tv_username.setText(pengguna.getUsername());

        holder.rl_pengguna.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                dialogEditHapus(pengguna, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return penggunas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_nama, tv_username, tv_level;
        public LinearLayout rl_pengguna;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_username = itemView.findViewById(R.id.tv_username);
            tv_level = itemView.findViewById(R.id.tv_level);
            rl_pengguna = itemView.findViewById(R.id.rl_pengguna);
        }
    }

    private void dialogEditHapus(final Pengguna pengguna, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        String[] listMenu = {"Edit", "Hapus"};

        builder.setTitle("Aksi")
                .setItems(listMenu, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                dialogEdit(pengguna);
                                break;
                            case 1:
                                dialogHapus(pengguna, position);
                                break;
                            default:
                                Toast.makeText(mContext, "Pilihan Salah", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
        builder.create();
        builder.show();
    }

    private void dialogEdit(final Pengguna pengguna) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        final View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_pengguna_baru,null);

        final EditText txt_nama = view.findViewById(R.id.txt_nama);
        final EditText txt_username = view.findViewById(R.id.txt_username);

        txt_nama.setText(pengguna.getNama());
        txt_username.setText(pengguna.getUsername());

        builder.setTitle("Edit Pengguna")
                .setView(view)
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Pengguna dataPengguna = new Pengguna();
                        dataPengguna.setId(pengguna.getId());
                        dataPengguna.setNama(txt_nama.getText().toString());
                        dataPengguna.setUsername(txt_username.getText().toString());
                        dataPengguna.setLevel(pengguna.getLevel());

                        mPresenter.editData(dataPengguna);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create();
        builder.show();
    }

    private void dialogHapus(final Pengguna pengguna, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Aksi")
                .setMessage("Apakah anda yankin ingin menghapus data ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.hapusData(pengguna);
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create();
        builder.show();
    }
}
