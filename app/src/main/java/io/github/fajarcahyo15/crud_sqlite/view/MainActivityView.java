package io.github.fajarcahyo15.crud_sqlite.view;

import io.github.fajarcahyo15.crud_sqlite.adapter.PenggunaAdapter;

/**
 * Created by root on 4/25/18.
 */

public interface MainActivityView extends View {

    void showData(PenggunaAdapter penggunaAdapter);

    void onAddData();

    void onAddedData(boolean sukses);

    void onUpdateData(boolean sukes);

    void onDeleteData(boolean sukes);
}
