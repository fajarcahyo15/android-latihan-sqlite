package io.github.fajarcahyo15.crud_sqlite.presenter;

import android.content.Context;

import io.github.fajarcahyo15.crud_sqlite.adapter.PenggunaAdapter;
import io.github.fajarcahyo15.crud_sqlite.database.PenggunaTableHandler;
import io.github.fajarcahyo15.crud_sqlite.model.Pengguna;
import io.github.fajarcahyo15.crud_sqlite.view.MainActivityView;

/**
 * Created by root on 4/25/18.
 */

public class MainActivityPresenter implements Presenter<MainActivityView> {

    private MainActivityView mainView;
    private PenggunaTableHandler penggunaTable;
    private Context mContext;
    private PenggunaAdapter penggunaAdapter;

    public MainActivityPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onAttach(MainActivityView view) {
        mainView = view;
    }

    @Override
    public void onDetach() {
        mainView = null;
    }

    public void tampilData() {
        penggunaTable = new PenggunaTableHandler(mContext);
        penggunaAdapter = new PenggunaAdapter(mContext, penggunaTable.getAll(), this);
        mainView.showData(penggunaAdapter);
    }

    public void tampilFilterData(String cari) {
        penggunaTable = new PenggunaTableHandler(mContext);
        penggunaAdapter = new PenggunaAdapter(mContext, penggunaTable.getFiltered(cari), this);
        mainView.showData(penggunaAdapter);
    }

    public void tambahData(Pengguna pengguna) {
        boolean sukses = false;
        penggunaTable = new PenggunaTableHandler(mContext);

        if (validasiInputTambah(pengguna)) {
            try {
                penggunaTable.save(pengguna);
                sukses = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mainView.onAddedData(sukses);
    }

    public void editData(Pengguna pengguna) {
        penggunaTable = new PenggunaTableHandler(mContext);
        boolean sukses = false;

        if (validInputUpdate(pengguna)) {
            try {
                penggunaTable.update(pengguna);
                sukses = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mainView.onUpdateData(sukses);
    }

    public void hapusData(Pengguna pengguna) {
        penggunaTable = new PenggunaTableHandler(mContext);
        boolean sukses = false;
        try {
            penggunaTable.delete(pengguna);
            sukses = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        mainView.onDeleteData(sukses);
    }

    private boolean validasiInputTambah(Pengguna pengguna) {
        boolean valid = true;

        if (pengguna.getNama().equals("") || pengguna.getNama().equals(null)) {
            valid = false;
        } else if (pengguna.getUsername().equals("") || pengguna.getUsername().equals(null)) {
            valid = false;
        } else if (pengguna.getLevel().equals("") || pengguna.getUsername().equals(null)) {
            valid = false;
        }

        return valid;
    }

    private boolean validInputUpdate(Pengguna pengguna) {
        boolean valid = true;

        if (!validasiInputTambah(pengguna)) {
            valid = false;
        } else if (String.valueOf(pengguna.getId()).equals(null)) {
            valid = false;
        }

        return valid;
    }
}
