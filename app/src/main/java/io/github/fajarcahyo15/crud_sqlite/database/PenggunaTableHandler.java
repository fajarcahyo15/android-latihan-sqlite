package io.github.fajarcahyo15.crud_sqlite.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.github.fajarcahyo15.crud_sqlite.helper.database.DatabasePropertyName;
import io.github.fajarcahyo15.crud_sqlite.model.Pengguna;

/**
 * Created by root on 4/25/18.
 */

public class PenggunaTableHandler extends SQLiteOpenHelper {

    private static final String TABLE_PENGGUNA = "pengguna";

    private static final String KEY_ID = "id";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_LEVEL = "level";

    public PenggunaTableHandler(Context context) {
        super(context, DatabasePropertyName.DATABASE_NAME, null, DatabasePropertyName.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTableQuery = "CREATE TABLE " + TABLE_PENGGUNA +
                " (" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_NAMA +" TEXT, " +
                KEY_USERNAME + " TEXT, " +
                KEY_LEVEL + " TEXT" +
                ")";
        sqLiteDatabase.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PENGGUNA);

        onCreate(sqLiteDatabase);
    }

    // aksi simpan data
    public void save(Pengguna pengguna) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAMA, pengguna.getNama());
        values.put(KEY_USERNAME, pengguna.getUsername());
        values.put(KEY_LEVEL, pengguna.getLevel());

        db.insert(TABLE_PENGGUNA, null, values);
        db.close();
    }

    // aksi mendapatkan data berdasarkan id
    public Pengguna getOneById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Pengguna pengguna = new Pengguna();

        Cursor cursor = db.query(TABLE_PENGGUNA, new String[]{KEY_ID,KEY_NAMA,KEY_USERNAME,KEY_LEVEL},
                KEY_ID+"=?", new String[]{String.valueOf(id)},null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        pengguna.setId(Integer.parseInt(cursor.getString(0)));
        pengguna.setNama(cursor.getString(1));
        pengguna.setUsername(cursor.getString(2));
        pengguna.setLevel(cursor.getString(3));

        return pengguna;
    }

    // mendapatkan semua data
    public List<Pengguna> getAll() {
        List<Pengguna> listPengguna = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectAllQuery = "SELECT * FROM " + TABLE_PENGGUNA;
        Cursor cursor = db.rawQuery(selectAllQuery,null);

        if (cursor.moveToFirst()) {
            do {
                Pengguna pengguna = new Pengguna();
                pengguna.setId(Integer.parseInt(cursor.getString(0)));
                pengguna.setNama(cursor.getString(1));
                pengguna.setUsername(cursor.getString(2));
                pengguna.setLevel(cursor.getString(3));
                listPengguna.add(pengguna);
            } while (cursor.moveToNext());
        }

        return listPengguna;
    }

    // filter pengguna
    public List<Pengguna> getFiltered(String cari) {
        List<Pengguna> listPengguna = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectFilteredQuery = "SELECT * FROM "+TABLE_PENGGUNA+" WHERE "+
                KEY_ID+" LIKE '%"+cari+"%' OR "+
                KEY_NAMA+" LIKE '%"+cari+"%' OR "+
                KEY_USERNAME+" LIKE '%"+cari+"%' OR "+
                KEY_LEVEL+" LIKE '%"+cari+"%'";

        Cursor cursor = db.rawQuery(selectFilteredQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Pengguna mPengguna = new Pengguna();
                mPengguna.setId(Integer.parseInt(cursor.getString(0)));
                mPengguna.setNama(cursor.getString(1));
                mPengguna.setUsername(cursor.getString(2));
                mPengguna.setLevel(cursor.getString(3));
                listPengguna.add(mPengguna);
            } while (cursor.moveToNext());
        }

        return listPengguna;
    }

    // aksi update data
    public void update(Pengguna pengguna) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAMA, pengguna.getNama());
        values.put(KEY_USERNAME, pengguna.getUsername());
        values.put(KEY_LEVEL, pengguna.getLevel());

        db.update(TABLE_PENGGUNA, values, KEY_ID+"=?",new String[]{String.valueOf(pengguna.getId())});
        db.close();
    }

    // aksi hapus data
    public void delete(Pengguna pengguna) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PENGGUNA, KEY_ID+"=?",new String[]{String.valueOf(pengguna.getId())});
        db.close();
    }
}
