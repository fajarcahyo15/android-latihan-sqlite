package io.github.fajarcahyo15.crud_sqlite.model;

/**
 * Created by root on 4/25/18.
 */

public class Pengguna {

    int id;
    String nama;
    String username;
    String level;

    public Pengguna() {}

    public Pengguna(int id, String nama, String username, String level) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
