package io.github.fajarcahyo15.crud_sqlite.presenter;

import io.github.fajarcahyo15.crud_sqlite.view.View;

/**
 * Created by root on 4/25/18.
 */

public interface Presenter<T extends View> {
    void onAttach(T view);

    void onDetach();
}
