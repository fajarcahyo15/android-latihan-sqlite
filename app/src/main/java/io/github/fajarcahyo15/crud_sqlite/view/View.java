package io.github.fajarcahyo15.crud_sqlite.view;

/**
 * Created by root on 4/25/18.
 */

public interface View {
    void onAttachView();

    void onDetachView();
}
