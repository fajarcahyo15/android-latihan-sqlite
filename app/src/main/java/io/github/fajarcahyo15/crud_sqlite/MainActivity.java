package io.github.fajarcahyo15.crud_sqlite;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.github.fajarcahyo15.crud_sqlite.adapter.PenggunaAdapter;
import io.github.fajarcahyo15.crud_sqlite.database.PenggunaTableHandler;
import io.github.fajarcahyo15.crud_sqlite.model.Pengguna;
import io.github.fajarcahyo15.crud_sqlite.presenter.MainActivityPresenter;
import io.github.fajarcahyo15.crud_sqlite.view.MainActivityView;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    MainActivityPresenter mainPresenter;
    RecyclerView rv_pengguna;
    EditText txt_cari;
    Button btn_cari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainActivityPresenter(this);
        rv_pengguna = findViewById(R.id.rv_pengguna);
        onAttachView();

        try {
            mainPresenter.tampilData();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Terjadi kesalahan saat memuat data!", Toast.LENGTH_SHORT).show();
        }

        btn_cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cari = txt_cari.getText().toString();
                mainPresenter.tampilFilterData(cari);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }


    @Override
    public void onAttachView() {
        mainPresenter.onAttach(this);
        getSupportActionBar().setTitle("Data Pengguna");
        getSupportActionBar().setSubtitle("Latihan CRUD SQLite");

        txt_cari = findViewById(R.id.txt_cari);
        btn_cari = findViewById(R.id.btn_cari);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_tambah:
                onAddData();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetachView() {
        mainPresenter.onDetach();
    }

    @Override
    public void showData(PenggunaAdapter penggunaAdapter) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_pengguna.setLayoutManager(layoutManager);
        rv_pengguna.setAdapter(penggunaAdapter);
    }

    @Override
    public void onAddData() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_pengguna_baru, null);


        builder.setTitle("Tambah Pengguna")
                .setView(dialogView)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        EditText txt_nama = dialogView.findViewById(R.id.txt_nama);
                        EditText txt_username = dialogView.findViewById(R.id.txt_username);
                        // Spinner spin_level = dialogView.findViewById(R.id.spin_level);

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, new String[]{"ADMIN", "SUPERADMIN"});
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // spin_level.setAdapter(arrayAdapter);

                        Pengguna pengguna = new Pengguna();
                        pengguna.setNama(txt_nama.getText().toString());
                        pengguna.setUsername(txt_username.getText().toString());
                        pengguna.setLevel("ADMIN");
                        mainPresenter.tambahData(pengguna);


                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create();
        builder.show();
    }

    @Override
    public void onAddedData(boolean sukses) {

        if (sukses) {
            mainPresenter.tampilData();
            Toast.makeText(this, "Berhasil menambah pengguna baru", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onUpdateData(boolean sukses) {
        if (sukses) {
            Toast.makeText(this, "Berhasil mengedit pengguna", Toast.LENGTH_SHORT).show();
            mainPresenter.tampilData();
        } else if (!sukses) {
            Toast.makeText(this, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDeleteData(boolean sukses) {
        if (sukses) {
            Toast.makeText(this, "Pengguna berhasil dihapus", Toast.LENGTH_SHORT).show();
            mainPresenter.tampilData();
        } else if (!sukses) {
            Toast.makeText(this, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
        }
    }


}
